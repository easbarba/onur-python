<!--
Onur is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Onur is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Onur. If not, see <https://www.gnu.org/licenses/>.
-->

# CHANGELOG

## 0.5.1

- feat: refactor, bump deps and improve cli ui

## 0.5.0

- feat: configuration organized per sub topic

## 0.4.1

- feat: reset and pull rebase repositories
- feat: actions do all hard work
- feat: log miscellaneous information

## 0.4.0

- chore: venv-based project

## 0.3.0

- feat: grab command pulls or clone repositories
- feat: display verbose output if requested
- feat: add user settings tests

## 0.2.0

- feat: config files discovery
- feat: parse one configuration file
- feat: parse all configuration files found
- feat: parse config as Config object

## 0.1.0

- initial structure
