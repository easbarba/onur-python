# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

FROM python:3.12

MAINTAINER EAS Barbosa <easbarba@outlook.com>
LABEL version=$ONUR_VERSION
LABEL description="Easily manage multiple FLOSS repositories."

ENV NEWUSER easbarba
ENV APP_HOME /home/$NEWUSER/app
ENV PATH "/home/$NEWUSER/.local/bin:$PATH"
RUN useradd -ms /bin/bash $NEWUSER # && chown -R $USER_NAME:$USER_NAME /home/$NEWUSER
USER $NEWUSER
WORKDIR $APP_HOME

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

COPY ./pyproject.toml .
RUN pip install --user --upgrade pip && \
    pip install --user --no-cache-dir .

COPY examples examples
COPY prepare.bash .
RUN ./prepare.bash

COPY . .

CMD [ "pytest" ]
