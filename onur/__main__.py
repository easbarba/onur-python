# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

"""Its show time."""

import argparse

from .commands import grab, archive


# ================================= CLI


parser = argparse.ArgumentParser(
    prog="Onur", description="Easily manage multiple FLOSS repositories."
)

parser.add_argument(
    "-i", "--verbose", help="provide additional information", action="store_true"
)
parser.add_argument("-v", "--version", action="version", version="%(prog)s 0.5.0")

subparsers = parser.add_subparsers(dest="command", help="Available subcommands")
subparsers.add_parser("grab", aliases=["g"], help="grab all projects")
archive_subcommand = subparsers.add_parser(
    "archive",
    aliases=["a"],
    help="archive projects",
)
archive_subcommand.add_argument("projects", help="comma-separated list of projects")

args = parser.parse_args()


def main() -> None:
    """Lets just do it."""
    if args.command is None:
        parser.print_help()
        exit(1)

    try:
        if args.command in ["grab", "g"]:
            grab.Grab(args.verbose).run()
            return

        if args.command in ["archive", "a"]:
            archive.Archive(args.items).run()
            return

        parser.print_help()
    except KeyboardInterrupt:
        print("Cancelling!")
        exit(1)


if __name__ == "__main__":
    main()
