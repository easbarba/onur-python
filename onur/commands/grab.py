# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

"""Grab all projects."""

from typing import Dict

from pathlib import Path

from termcolor import colored

from onur.database import parse
from onur.misc import info
from onur.misc import settings
from onur.actions import klone, pull
from onur.models import project, config


class Grab:
    """..."""

    def __init__(self, verbose: bool) -> None:
        """..."""
        self.all_configs: list[config.Config] = parse.Parse().multi()
        self.projects_dir: Path = info.projects_dir
        self.settings: dict[str, str | bool] = settings.values()
        self.verbose = verbose

    def run(self) -> None:
        """..."""

        for _config in self.all_configs:
            print(f"""> {colored(_config.name.capitalize(), 'yellow')}""")

            for topic, projects in _config.projects.items():
                print(f"""  + {colored(topic.capitalize(), 'green')}""")

                for _project in projects:
                    filepath: Path = Path(self.projects_dir.joinpath(_config.name))
                    if len(_config.projects.keys()) > 1:
                        filepath = filepath.joinpath(topic)

                    filepath = filepath.joinpath(_project.name)

                    print(self.__info(_project, str(filepath)))

                    if self.__repo_exists(filepath):
                        pull.Pull(filepath).run()
                    else:
                        klone.Klone(filepath, _project, self.__options()).run()

                print()

    def __repo_exists(self, filepath: Path) -> bool:
        """."""
        return filepath.joinpath(".git", "config").exists()

    def __info(self, project: project.Project, folder: str) -> str:
        """Collect running project information."""

        return f"    - {project.name.ljust(35)} {colored(project.url.ljust(75), 'blue')} {project.branch}"

    def __options(self) -> list[str]:
        """.."""
        options: list[str] = []

        if self.settings.get("quiet") and self.settings.get("quiet") is False:
            options.append("--progress")

        if self.settings.get("depth"):
            options.append(f"--depth={self.settings.get('depth')}")

        if self.settings.get("single-branch") and self.settings.get("quiet") is True:
            options.append("--single-branch")

        return options
