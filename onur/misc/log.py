# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

"""Log miscellaneous information."""

import logging

from .info import config_dir


def loggin():
    """..."""
    logging.basicConfig(
        filename=config_dir.joinpath("onur.log"),
        encoding="utf-8",
        level=logging.INFO,
    )


def info(message: str):
    """..."""
    loggin()
    logging.info(message)


def warning(message: str):
    """..."""
    loggin()
    logging.warning(message)


def error(message: str):
    """..."""
    loggin()
    logging.error(message)
