# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

"""."""

import json
from pathlib import Path
from typing import Dict, Any, Union

from onur.models import project, config
from onur.misc import info
from . import repository


class Parse:
    """Parse configuration files."""

    def __init__(self):
        """..."""
        self.config_dir: Path = info.config_dir
        self.files: repository.Files = repository.Files()

    def single(self, filepath: Path) -> config.Config:
        """Parse file to a configuration object."""
        with open(str(filepath), "rb") as json_raw:
            data: Any = json.load(json_raw)

            result: config.Config = config.Config(
                filepath.stem,
                {
                    key: [
                        self.to_project(_project)
                        for _project in value
                        if self.to_project(_project) is not False
                    ]
                    for key, value in data.items()
                },
            )

            print(f" {result.name} ", end="")

        return result

    def multi(self) -> list[config.Config]:
        """Bundle all configuration."""
        print("Configurations: [ ", end="")

        configs: list[config.Config] = []  # TODO: use Collection instead

        for config_current in self.files.namespath():
            config_path: Path = self.config_dir.joinpath(config_current)
            configs.append(self.single(config_path))

        print(" ]\n")
        return configs

    def to_project(self, _project: Dict[str, str]) -> Union[project.Project, False]:
        """Return a Project out of dict, branch defaulting to master, or false if corrupted."""
        if _project.get("name") is None or _project.get("url") is None:
            return False

        return project.Project(
            name=_project.get("name"),
            url=_project.get("url"),
            branch=_project.get("branch", "master"),
        )
