# Onur is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Onur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Onur. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf.

.DEFAULT_GOAL := tests

RUNNER ?= podman

NAME := onur-python
VERSION := $(shell gawk '/version/ {version=substr($$3, 2,5); print version}' pyproject.toml )
CONTAINER_IMAGE := registry.gitlab.com/${USER}/${NAME}:${VERSION}

# ----------------------------- TASKS

.phony: prepare
prepare:
	./prfix.bash

.phony: local.install
local.install:
	python3 -m pip install . --break-system-packages

.phony: local.uninstall
local.uninstall:
	python3 -m pip uninstall onur --break-system-packages

.PHONY: local.env
local.env:
	python -m venv venv
	bash ./venv/bin/activate
	./venv/bin/pip install --upgrade pip
	./venv/bin/pip install .

.PHONY: local.commands
local.commands:
	cat ./commands | fzf

.PHONY: local.clean
local.clean:
	rm -rf ./venv ./build ./dist ./.pytest_cache ./onur.egg-info 
	find ./onur -type d -name '__pycache__' -exec rm -r {} +

# ------------------------------- CONTAINER

.PHONY: image.build
image.build:
	${RUNNER} build \
		--file ./Containerfile \
		--tag ${CONTAINER_IMAGE} \
		--env ONUR_VERSION=${VERSION}

.PHONY: image.repl
image.repl:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		--workdir /home/easbarba/app \
		${CONTAINER_IMAGE} bash

.PHONY: image.publish
image.publish:
	${RUNNER} push ${CONTAINER_IMAGE}

.PHONY: test
test:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		--workdir /home/easbarba/app \
		${CONTAINER_IMAGE} bash -c "meson test -C build"

.PHONY: image.commands
image.commands:
	${RUNNER} run --rm -it \
		--volume ${PWD}:/app:Z \
		--workdir /home/easbarba/app \
		${CONTAINER_IMAGE} bash -c "$(shell cat ./commands | fzf)"
